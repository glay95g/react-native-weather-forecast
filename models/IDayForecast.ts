import { DateUserViewFormat } from '../services/DateHelper';

interface IDayForecast {
  date: DateUserViewFormat,
  temperature: number,
  weather: {
    title: string,
    iconId: string
  }
}

export default IDayForecast;
