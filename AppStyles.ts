import commonStyleVariables from "./styles/commonStyleVariables";
import { StyleSheet } from "react-native";

const AppStyles = StyleSheet.create({
  app: {
    display: "flex",
    flex: 1,
    backgroundColor: commonStyleVariables.colorAccent,
    alignItems: "center",
    justifyContent: "space-between",
    flexDirection: "column",
    minHeight: "100vh",
    minWidth: commonStyleVariables.contentMinWidth,
    fontFamily: "Ubuntu",
  },
  app__header: {
    backgroundColor: "#373af5",
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "column",
    width: "100%",
    height: 100,
    margin: 0,
    color: "white",
  },
  app__title: {
    backgroundColor: "#373af5",
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "column",
    margin: 0,
    color: "white",
    fontSize: 40,
    fontStyle: "normal",
    fontWeight: "bold",
  },
  app__main: {
    display: "flex",
    flexWrap: "wrap",
    flexDirection: "row",
    width: "100%",
  },
  app__forecastColumn: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    padding: "10px",
  },
  app__forecastColumn_mobile: { width: "100%" },
  app__forecastColumn_desktop: { width: "50%" },
  app__footer: {
    width: "100%",
    textAlign: "center",

    fontFamily: "Ubuntu",
    fontStyle: "normal",
    fontWeight: "normal",
    fontSize: 14,
    lineHeight: 18,

    color: commonStyleVariables.colorBaseWeak,

    opacity: 0.6,

    marginVertical: "16px",
  },
});

export default AppStyles;
