const commonStyleVariables = {
  //SIZES
  //Component sizes
  contentMinWidth: "320px",
  contentMinHeight: "767px",

  contentTabletWidth: "1024px",
  contentMobileWidth: "600px",

  //COLORS
  colorBase: "#2c2d76",
  colorSecondary: "#8083a4",
  colorAccent: "#373af5",
  colorCritic: "#e5596d",
  colorBaseWeak: "#ffffff",
};

export default commonStyleVariables;
