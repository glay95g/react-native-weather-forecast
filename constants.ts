import ICity from './models/ICity';

const cities: ICity[] = [{
  id: 0,
  name: 'Самара',
  latitude: 53.195873,
  longitude: 50.100193,
}, {
  id: 1,
  name: 'Тольятти',
  latitude: 53.507836,
  longitude: 49.420393,
}, {
  id: 2,
  name: 'Саратов',
  latitude: 51.533557,
  longitude: 46.034257,
}, {
  id: 3,
  name: 'Казань',
  latitude: 55.796127,
  longitude: 49.106405,
}, {
  id: 4,
  name: 'Краснодар',
  latitude: 45.035470,
  longitude: 38.975313,
}];

export default cities;
