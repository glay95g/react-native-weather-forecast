import commonStyleVariables from "../../styles/commonStyleVariables";
import { StyleSheet } from "react-native";

const BaseSelectStyles = StyleSheet.create({
  selectComponent: {
    position: "relative",
    width: "252px",
    minWidth: "252px",
    height: "48px",

    borderStyle: "solid",
    borderWidth: 2,
    borderColor: commonStyleVariables.colorSecondary,
    borderRadius: 8,

    marginVertical: "5px",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  selectComponent__select: {
    width: "100%",

    color: commonStyleVariables.colorBase,

    borderRadius: 8,

    fontFamily: "Ubuntu",
    fontStyle: "normal",
    fontWeight: "normal",
    fontSize: 16,
    lineHeight: 24,

    paddingRight: "46px",
    paddingLeft: "16px",
  },
});

export default BaseSelectStyles;
