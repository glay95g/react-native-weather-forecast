import React, { ChangeEvent, useState } from 'react';
import { View } from 'react-native';
import IBaseObject from '../../models/IBaseObject';
import type { PickerItem } from 'react-native-woodpicker'
import { Picker } from 'react-native-woodpicker'
import BaseSelectStyles from './BaseSelectStyles';

interface BaseSelectProps extends React.InputHTMLAttributes<HTMLInputElement> {
	placeholder: string,
	options: IBaseObject[],
	onChangeOption: (option: IBaseObject) => void
}

function BaseSelect(props: BaseSelectProps): JSX.Element {
	const [pickedData, setPickedData] = useState<PickerItem>(null);
	const { placeholder, options, onChangeOption } = props;

	const optionsList = options?.map(
		(option: IBaseObject): PickerItem => { return { label: option.name, value: option.id } },
	);

	const optionChanged = (pickerItem: PickerItem, pickerItemIndex: number) => {
		const foundOption: IBaseObject | undefined = options[pickerItemIndex];
		setPickedData(optionsList[pickerItemIndex]);
		if (foundOption) {
			onChangeOption(foundOption);
		}
	};

	return (
		<View style={BaseSelectStyles.selectComponent}>
			<Picker
				containerStyle={BaseSelectStyles.selectComponent__select}
				textInputStyle={{ width: "100%" }}
				item={pickedData}
				items={optionsList}
				onItemChange={optionChanged}
				placeholder={placeholder}
			/>
		</View>
	);
}

export default BaseSelect;
