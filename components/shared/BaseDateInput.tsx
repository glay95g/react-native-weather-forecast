import React, { useState } from 'react';
import { View } from 'react-native';
import { DatePicker } from 'react-native-woodpicker'
import BaseSelectStyles from './BaseSelectStyles';

interface BaseDateInputProps extends React.InputHTMLAttributes<HTMLInputElement> {
	placeholder: string,
	minDate: Date,
	maxDate: Date,
	onChangeDate: (date: Date) => void
}

function BaseDateInput(props: BaseDateInputProps): JSX.Element {
	const {
		placeholder, minDate, maxDate, onChangeDate,
	} = props;

	const [pickedDate, setPickedDate] = useState<Date>();

	const datePickerText = (): string => pickedDate
		? pickedDate.toDateString()
		: placeholder;

	const dateChanged = (selectedDate: Date) => {
		setPickedDate(selectedDate);
		onChangeDate(selectedDate);
	};

	return (
		<View style={BaseSelectStyles.selectComponent} >
			<DatePicker
				containerStyle={BaseSelectStyles.selectComponent__select}
				textInputStyle={{ width: "100%" }}
				value={pickedDate}
				onDateChange={dateChanged}
				text={datePickerText()}
				minimumDate={minDate}
				maximumDate={maxDate}
			/>
		</View>
	);
}

export default BaseDateInput;
