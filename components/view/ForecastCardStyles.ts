import commonStyleVariables from "../../styles/commonStyleVariables";
import { StyleSheet } from "react-native";

const ForecastCardStyles = StyleSheet.create({
  forecastCard: {
    width: "100%",
    maxWidth: "660px",
    minWidth: "300px",

    maxHeight: "524px",
    minHeight: "524px",

    backgroundColor: commonStyleVariables.colorBaseWeak,
    borderRadius: 8,

    display: "flex",
    flexDirection: "column",
  },
  forecastCard__header: {
    flexGrow: 1,
    display: "flex",
    flexDirection: "column",
    alignItems: "flex-start",
    justifyContent: "flex-start",
    marginHorizontal: "24px",
    marginTop: "32px",
    marginBottom: "24px",
  },

  forecastCard__title: {
    textAlign: "left",
    fontFamily: "Ubuntu",
    fontStyle: "normal",
    fontWeight: "bold",
    fontSize: 32,
    lineHeight: 32,
    margin: 0,
    display: "flex",
    alignItems: "flex-end",

    color: commonStyleVariables.colorBase,
  },
  forecastCard__searchBar: {
    flexGrow: 2,
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    flexWrap: "wrap",
    flexDirection: "row",
    marginVertical: "10px",
    marginHorizontal: "24px",
  },

  forecastCard__information: {
    flexGrow: 4,

    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    minHeight: "240px",
    width: "100%",
    paddingHorizontal: "24px",
    marginTop: "10px",
    marginBottom: "32px",
  },

  forecastCard__placeholder: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
  },

  forecastCard__placeholderImage: {
    width: "160px",
    height: "160px",
  },

  forecastCard__placeholderTitle: {
    fontFamily: "Ubuntu",
    fontStyle: "normal",
    fontWeight: "bold",
    fontSize: 16,
    lineHeight: 24,
    textAlign: "center",

    color: commonStyleVariables.colorSecondary,
  },
});

export default ForecastCardStyles;
