/* eslint-disable jsx-a11y/control-has-associated-label */
import React from 'react';
import forecastPlaceholderImage from '../../assets/placeholder/forecast-placeholder.svg';
import BaseSelect from '../shared/BaseSelect';
import BaseDateInput from '../shared/BaseDateInput';
import WeatherCard from './WeatherCard';
import DateHelper, { DateSystemViewFormat } from '../../services/DateHelper';
import ICity from '../../models/ICity';
import IDayForecast from '../../models/IDayForecast';
import IBaseObject from '../../models/IBaseObject';
import { Image, ScrollView, Text, View } from 'react-native';
import ForecastCardStyles from './ForecastCardStyles';

interface ForecastCardProps extends React.InputHTMLAttributes<HTMLInputElement> {
	isSingleDateForecast: boolean,
	cities: ICity[],
	forecastData: IDayForecast | IDayForecast[] | null,
	isLoading: boolean,
	changeSelectedCity: (cityOption: ICity, isSingleDateForecast?: boolean) => void,
	changeSelectedDate: (date: Date) => void
}

function ForecastCard(props: ForecastCardProps): JSX.Element {
	const {
		isSingleDateForecast, cities, forecastData, isLoading, changeSelectedCity, changeSelectedDate,
	} = props;

	const todayDate: Date = new Date();
	todayDate.setTime(todayDate.getTime() - DateHelper.oneDayOffset);
	const yesterdayDateString: DateSystemViewFormat = DateHelper.getDateString(todayDate);
	const fiveDaysAgoDate: Date = new Date();
	fiveDaysAgoDate.setTime(fiveDaysAgoDate.getTime() - DateHelper.oneDayOffset * 5);
	const fiveDaysAgoDateString: DateSystemViewFormat = DateHelper.getDateString(fiveDaysAgoDate);

	let forecastView;
	if (isSingleDateForecast) {
		forecastView = forecastData ? (
			<View style={{ marginVertical: '10px', width: '100%' }}>
				<WeatherCard
					isAdaptiveWidth
					dayForecast={forecastData as IDayForecast}
					isLoading={isLoading}
				/>
			</View>
		) : null;
	} else {
		forecastView = forecastData ? (
			<View style={{ margin: "5px", width: "100%" }}>
				<ScrollView horizontal={true}>
					{forecastData && (forecastData as IDayForecast[]).map((f: IDayForecast) => (
						<View key={f.date} style={{ margin: "5px" }}>
							<WeatherCard
								isAdaptiveWidth={false}
								dayForecast={f}
								isLoading={isLoading}
							/>
						</View>
					))}
				</ScrollView>
			</View>
		)
			: null;
	}

	const placeholderView = (!forecastData && (
		<View style={ForecastCardStyles.forecastCard__placeholder} >
			<Image style={ForecastCardStyles.forecastCard__placeholderImage} source={{
				uri: forecastPlaceholderImage,
			}} />
			<Text style={ForecastCardStyles.forecastCard__placeholderTitle}>
				Fill in all the fields and the weather will be displayed
    </Text>
		</View>
	));

	return (
		<View style={ForecastCardStyles.forecastCard}>
			<View style={ForecastCardStyles.forecastCard__header}>
				<Text style={ForecastCardStyles.forecastCard__title}>{isSingleDateForecast ? 'Forecast for a Date in the Past' : '7 Days Forecast'}</Text>
			</View>
			<View style={ForecastCardStyles.forecastCard__searchBar}>
				<BaseSelect
					placeholder="Select city"
					options={cities}
					onChangeOption={
						(city: IBaseObject) => changeSelectedCity(city as ICity, isSingleDateForecast)
					}
				/>
				{isSingleDateForecast && (
					<BaseDateInput
						placeholder="Select date"
						minDate={todayDate}
						maxDate={fiveDaysAgoDate}
						onChangeDate={changeSelectedDate}
					/>
				)}
			</View>
			<View style={ForecastCardStyles.forecastCard__information}>
				{placeholderView}
				{forecastView}
			</View>
		</View>
	);
}

export default React.memo(ForecastCard);
