import React from 'react';
import { View, Image, Text } from 'react-native';
import IDayForecast from '../../models/IDayForecast';
import WeatherApiService from '../../services/WeatherApiService';
import WeatherCardStyles from './WeatherCardStyles';

interface WeatherCardProps extends React.InputHTMLAttributes<HTMLInputElement> {
	isAdaptiveWidth: boolean,
	isLoading: boolean,
	dayForecast: IDayForecast,
}

function WeatherCard(props: WeatherCardProps): JSX.Element {
	const { isAdaptiveWidth, isLoading, dayForecast } = props;

	const dayForecastTemperatureView = dayForecast ? (`${(dayForecast.temperature > 0 ? '+' : '') + dayForecast.temperature}°`) : '';

	const weatherCardAdaptiveWidthClass = isAdaptiveWidth ? WeatherCardStyles.weatherCard_adaptiveWidth : undefined;
	const weatherCardALoadingClass = isLoading ? WeatherCardStyles.weatherCard_loading : undefined;
	const weatherCardAllStyles = [WeatherCardStyles.weatherCard, weatherCardAdaptiveWidthClass, weatherCardALoadingClass];
	const weatherIconURL = WeatherApiService.getWeatherIconURL(dayForecast.weather.iconId);

	return (
		<View style={weatherCardAllStyles}>
			<View style={WeatherCardStyles.weatherCard__top}>
				<Text style={WeatherCardStyles.weatherCard__date}>{dayForecast?.date ?? ''}</Text>
			</View>
			<View style={WeatherCardStyles.weatherCard__middle}>
				{!!dayForecast && (
					< Image
						style={WeatherCardStyles.weatherCard__weatherIcon}
						source={{
							uri: weatherIconURL,
						}}
					/>
				)}
			</View>
			<View style={WeatherCardStyles.weatherCard__bottom}>
				<Text style={WeatherCardStyles.weatherCard__temperature}>{dayForecastTemperatureView}</Text>
			</View>
		</View>
	);
}

export default React.memo(WeatherCard);
