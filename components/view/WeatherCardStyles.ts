import commonStyleVariables from "../../styles/commonStyleVariables";
import { StyleSheet } from "react-native";

const WeatherCardStyles = StyleSheet.create({
  weatherCard: {
    width: "100%",
    maxWidth: "174px",
    minWidth: "174px",

    height: "238px",
    maxHeight: "238px",
    minHeight: "238px",

    backgroundColor: commonStyleVariables.colorAccent,
    borderRadius: 8,

    alignItems: "center",
    justifyContent: "space-between",
  },
  weatherCard_adaptiveWidth: {
    maxWidth: "100%",
  },
  weatherCard_loading: {
    opacity: 0.7,
  },

  weatherCard__top: {
    flexGrow: 1,
    display: "flex",
    alignItems: "flex-start",
    justifyContent: "flex-start",
  },
  weatherCard__date: {
    textAlign: "left",

    fontFamily: "Ubuntu",
    fontStyle: "normal",
    fontWeight: "bold",
    fontSize: 16,
    lineHeight: 24,

    marginHorizontal: "20px",
    marginTop: "20px",
    marginBottom: "15px",

    color: commonStyleVariables.colorBaseWeak,
  },
  weatherCard__middle: {
    flexGrow: 3,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",

    position: "relative",
  },
  weatherCard__weatherIcon: {
    width: "120px",
    height: "120px",
  },
  weatherCard__bottom: {
    flexGrow: 1,

    display: "flex",
    alignItems: "flex-end",
    justifyContent: "flex-end",
  },

  weatherCard__temperature: {
    textAlign: "right",

    fontFamily: "Ubuntu",
    fontStyle: "normal",
    fontWeight: "bold",
    fontSize: 32,
    lineHeight: 32,

    marginHorizontal: "20px",
    marginTop: "15px",
    marginBottom: "20px",

    color: commonStyleVariables.colorBaseWeak,
  },
});

export default WeatherCardStyles;
