import { Text, View, ViewStyle } from 'react-native';
import { useMediaQuery } from 'react-responsive';
import React, { useEffect, useState } from 'react';
import ForecastCard from './components/view/ForecastCard';
import ICity from './models/ICity';
import IDayForecast from './models/IDayForecast';
import DateHelper from './services/DateHelper';
import WeatherApiService from './services/WeatherApiService';
import cities from './constants';
import commonStyleVariables from './styles/commonStyleVariables';
import AppStyles from './AppStyles';


export default function App() {
	const [pastDayForcastSelectedCity, setPastDayForcastSelectedCity] = useState<ICity | null>(null);
	const [pastDayForcastSelectedDate, setPastDayForcastSelectedDate] = useState<number | null>(null);
	const [pastDayForecastData, setPastDayForecastData] = useState<IDayForecast | null>(null);
	const [isPastDayForecastData, setIsPastDayForecastDataLoading] = useState<boolean>(false);
	const [forecastCardsStyles, setForecastCardsStyles] = useState<ViewStyle[]>([AppStyles.app__forecastColumn]);

	const [sevenDaysForcastSelectedCity,
		setSevenDaysForcastSelectedCity] = useState<ICity | null>(null);
	const [sevenDaysForecastData,
		setSevenDaysForecastData] = useState<Array<IDayForecast> | null>(null);
	const [isSevenDaysForecastDataLoading,
		setIsSevenDaysForecastDataLoading] = useState<boolean>(false);

	const isTabletOrMobileDevice =
		useMediaQuery({
			query: `(max-width: ${commonStyleVariables.contentTabletWidth})`,
		});

	useEffect(() => {
		setForecastCardsStyles([
			AppStyles.app__forecastColumn,
			isTabletOrMobileDevice
				? AppStyles.app__forecastColumn_mobile
				: AppStyles.app__forecastColumn_desktop
		]);
	}, []);

	useEffect(() => {
		if (!!pastDayForcastSelectedCity && !!pastDayForcastSelectedDate) {
			setIsPastDayForecastDataLoading(true);
			WeatherApiService.getPastDayForecast(pastDayForcastSelectedCity,
				pastDayForcastSelectedDate, (data: IDayForecast | null) => {
					setPastDayForecastData(data);
					setIsPastDayForecastDataLoading(false);
				});
		}
	}, [pastDayForcastSelectedCity, pastDayForcastSelectedDate]);

	useEffect(() => {
		if (sevenDaysForcastSelectedCity) {
			setIsSevenDaysForecastDataLoading(true);
			WeatherApiService.getSevenDaysForecast(sevenDaysForcastSelectedCity,
				(data: IDayForecast[] | null) => {
					setSevenDaysForecastData(data);
					setIsSevenDaysForecastDataLoading(false);
				});
		}
	}, [sevenDaysForcastSelectedCity]);

	const changeSelectedCity = (city: ICity, isSingleDateForecast?: boolean) => {
		if (isSingleDateForecast) {
			setPastDayForcastSelectedCity(city);
		} else {
			setSevenDaysForcastSelectedCity(city);
		}
	};

	const changeSelectedDate = (selectedDate: Date) => {
		const selectedDateTimeUTC = DateHelper.convertDateTimeToUTCTime(selectedDate.getTime()
			+ Math.round(DateHelper.oneDayOffset / 2));
		setPastDayForcastSelectedDate(selectedDateTimeUTC);
	};

	return (
		<View style={AppStyles.app}>
			<View style={AppStyles.app__header}>
				<Text style={AppStyles.app__title}>
					Weather forecast
				</Text>
			</View>

			<View style={AppStyles.app__main}>
				<View style={forecastCardsStyles}>
					<ForecastCard
						isSingleDateForecast={false}
						cities={cities}
						forecastData={sevenDaysForecastData}
						isLoading={isSevenDaysForecastDataLoading}
						changeSelectedCity={changeSelectedCity}
						changeSelectedDate={changeSelectedDate}
					/>
				</View>
				<View style={forecastCardsStyles}>
					<ForecastCard
						isSingleDateForecast
						cities={cities}
						forecastData={pastDayForecastData}
						isLoading={isPastDayForecastData}
						changeSelectedCity={changeSelectedCity}
						changeSelectedDate={changeSelectedDate}
					/>
				</View>
			</View>

			<Text style={AppStyles.app__footer}>
				C ЛЮБОВЬЮ ОТ MERCURY DEVELOPMENT
			</Text>
		</View>
	);
}